//
//  Shot.swift
//  DribbbleApp
//
//  Created by Leonardo Augusto N Saragiotto on 21/03/18.
//  Copyright © 2018 Leonardo Augusto N Saragiotto. All rights reserved.
//

import Foundation
import ObjectMapper

class Shot: Mappable {
    var id: Int?
    var title: String?
    var date: String?
    var description: String?
    var imageUrl: String?
    var imageUrlHD: String?
    var userName: String?
    
    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        date <- map["created_at"]
        description <- map["description"]
        imageUrl <- map["images.normal"]
        imageUrlHD <- map["images.hidpi"]
        userName <- map["user.name"]
    }
}
