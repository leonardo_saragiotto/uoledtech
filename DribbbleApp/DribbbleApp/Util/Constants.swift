//
//  Constants.swift
//  DribbbleApp
//
//  Created by Leonardo Augusto N Saragiotto on 21/03/18.
//  Copyright © 2018 Leonardo Augusto N Saragiotto. All rights reserved.
//

import Foundation

struct Constants {
    static let kDribbbleEndpoint = "https://api.dribbble.com/v1/shots"
    static let kToken = "b8499a6f9e605d38ae2a4aef5bb3201b6b7ef359a8c0a1f6f8534a202726fc6d"
    
    static let kResultsRequest = 12
    
    static let kShotListCellIdentifier = "ShotListCell"
    
    static let kShotListViewTitle = "Dribbble Shots"
    
    static let kShotSegueDetail = "showShotDetail"
}
