//
//  ShotListViewController.swift
//  DribbbleApp
//
//  Created by Leonardo Augusto N Saragiotto on 21/03/18.
//  Copyright © 2018 Leonardo Augusto N Saragiotto. All rights reserved.
//

import UIKit

class ShotListViewController: UITableViewController {

    var viewModel:ShotListViewModel?
    internal var refresh: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initViewController()
        
        self.initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    private func initViewController() {
        self.title = Constants.kShotListViewTitle
        
        tableView.tableFooterView = UIView()
        tableView.register(UINib.init(nibName: "ShotListViewCell", bundle: nil), forCellReuseIdentifier: Constants.kShotListCellIdentifier)
        
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refreshView), for: .valueChanged)
        tableView.addSubview(refresh)
    }
    
    private func initViewModel() {
        self.viewModel = ShotListViewModel()
        
        self.viewModel?.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.refresh.endRefreshing()
            }
        }
        
        self.viewModel?.insertItensTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.insertRows(at: self?.viewModel?.newItensIndexPath ?? [], with: .automatic)
            }
        }
        
        self.fetchShots()
    }
    
    private func fetchShots() {

        self.viewModel?.fetchShots()
    }
    
    @objc private func refreshView() {
        self.refresh.beginRefreshing()
        self.initViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.viewModel?.numberOfCells ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.kShotListCellIdentifier, for: indexPath) as! ShotListViewCell
        
        cell.configureWith(self.viewModel!.getShot(at: indexPath))
        
        if (indexPath.row == self.viewModel!.numberOfCells - 3) {
            self.fetchShots()
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: Constants.kShotSegueDetail, sender: nil)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.kShotSegueDetail {
            if let detailVC = segue.destination as? ShotDetailViewController {
                let shotViewModel = self.viewModel?.getShotDetail(at: self.tableView.indexPathForSelectedRow!)
                detailVC.shot = shotViewModel
            }
        }
    }
}
