//
//  ShotDetailViewController.swift
//  DribbbleApp
//
//  Created by Leonardo Augusto N Saragiotto on 22/03/18.
//  Copyright © 2018 Leonardo Augusto N Saragiotto. All rights reserved.
//

import UIKit

class ShotDetailViewController: UIViewController {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var shotDescriptionTextView: UITextView!
    @IBOutlet weak var shotImageView: UIImageView!
    
    var shot:ShotDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = shot!.title
        self.userNameLabel.text = shot!.userName
        
        if let data = shot!.description.data(using: String.Encoding.unicode) {
            let attrStr = try? NSAttributedString( 
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            
            self.shotDescriptionTextView.attributedText = attrStr
        } else {
            self.shotDescriptionTextView.text = shot!.description
        }
        
        if !shot!.imagePath.isEmpty {
            self.shotImageView.kf.setImage(with: URL(string: shot!.imagePath)!,
                                        options:[.transition(.fade(0.3))])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
