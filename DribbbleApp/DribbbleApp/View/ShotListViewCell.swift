//
//  ShotListViewCell.swift
//  DribbbleApp
//
//  Created by Leonardo Augusto N Saragiotto on 21/03/18.
//  Copyright © 2018 Leonardo Augusto N Saragiotto. All rights reserved.
//

import UIKit
import Kingfisher

class ShotListViewCell: UITableViewCell {

    @IBOutlet weak var imageShot: UIImageView!
    @IBOutlet weak var shotTitle: UILabel!
    @IBOutlet weak var shotUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWith(_ viewCellModel: ShotCellViewModel) {
        self.shotTitle.text = viewCellModel.title
        self.shotUserName.text = viewCellModel.userName
        
        self.imageShot.layer.cornerRadius = 5.0
        self.imageShot.layer.masksToBounds = true
        
        if !viewCellModel.imagePath.isEmpty {
            self.imageShot.kf.setImage(with: URL(string: viewCellModel.imagePath)!,
                                            options:[.transition(.fade(0.3))])
        }
    }
    
}
