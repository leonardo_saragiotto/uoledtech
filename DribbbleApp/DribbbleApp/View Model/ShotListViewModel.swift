//
//  ShotListViewModel.swift
//  DribbbleApp
//
//  Created by Leonardo Augusto N Saragiotto on 21/03/18.
//  Copyright © 2018 Leonardo Augusto N Saragiotto. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class ShotListViewModel {
    private var shots:[Shot] {
        didSet {
            (self.page == 1) ? self.reloadTableViewClosure?() : self.insertItensTableViewClosure?()
        }
    }
    private var page:Int
    var newItensIndexPath:[IndexPath]
    var numberOfCells: Int {
        return shots.count
    }
    
    var reloadTableViewClosure: (() -> ())?
    var insertItensTableViewClosure: (() -> ())?
    
    init() {
        page = 1
        shots = [Shot]()
        newItensIndexPath = [IndexPath]()
    }
    
    func fetchShots() {
        let url = "\(Constants.kDribbbleEndpoint)?page=\(page)per_page=\(Constants.kResultsRequest)&access_token=\(Constants.kToken)"
        
        Alamofire.request(url).responseArray { (response: DataResponse<[Shot]>) in
            
            switch response.result {
            case .success(let result):
                if (self.page > 1) {self.createIndexPathList(result)}
                self.shots += result
                self.page += 1
                break
            case .failure:
                print("Error requesting shots")
                break
            }
        }
    }
    
    private func createIndexPathList(_ shots:[Shot]) {
        var row = 0
        newItensIndexPath = [IndexPath]()
        _ = shots.map({ _ in
            newItensIndexPath.append(IndexPath(item: self.numberOfCells + row, section: 0))
            row += 1
        })
    }
    
    func getShot(at indexPath:IndexPath) -> ShotCellViewModel {
        let shot = self.shots[indexPath.row]
        
        return ShotCellViewModel(title: shot.title ?? "",
                                 imagePath: shot.imageUrl ?? "",
                                 userName: shot.userName ?? "")
        
    }
    
    func getShotDetail(at indexPath:IndexPath) -> ShotDetailViewModel {
        let shot = self.shots[indexPath.row]
        
        return ShotDetailViewModel(title: shot.title ?? "",
                                   description: shot.description ?? "",
                                   userName: shot.userName ?? "",
                                   imagePath: shot.imageUrlHD ?? shot.imageUrl ?? "")
    }
}

struct ShotCellViewModel {
    let title: String
    let imagePath: String
    let userName: String
}

struct ShotDetailViewModel {
    let title: String
    let description:String
    let userName:String
    let imagePath: String
}
